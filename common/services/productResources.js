(function(){
	"use strict";

	angular.module("common.services")
			.factory("productResources",["$resource", function($resource){
				return $resource("/api/products/:productId")
			}]);
}());